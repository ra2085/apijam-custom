# Apigee API Jam
This is the supporting material for a one-day hands-on workshop that introduces developers to Apigee Edge. We call this event an "**Apigee API Jam**".

All of the material here is released under the [Apache 2.0 license](./LICENSE.md)

### [Core Labs](./Labs/Core) 
1. API Design - Create a Reverse Proxy with OpenAPI specification
2. Traffic Management - Throttle APIs
3. API Security - Securing APIs with API Keys
4. API Diagnostics - Trace tool


### Useful Links

* [Apigee Management UI](https://login.apigee.com)
* [Mock URLs](https://bitbucket.org/ra2085/apijam-custom/src/0f731280c683d1e6c455cb8eea7ef33420ab94c6/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/Team/proxy_config.md)
* [MVBO Specs](https://bitbucket.org/ra2085/apijam-custom/src/0f731280c683d1e6c455cb8eea7ef33420ab94c6/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/Team/specs.md)
* [Developer Portal](http://test-fidelity-prod.devportal.apigee.io/user/login?destination=node/782)
* [Postman Test Setup](https://bitbucket.org/ra2085/apijam-custom/src/0f731280c683d1e6c455cb8eea7ef33420ab94c6/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/Team/postman_setup.md)

#### Apigee Community 
If you have any questions/comments please visit https://community.apigee.com/index.html

##### This is not an official Google or Apigee product. This repository is used for educational/training purposes only.
