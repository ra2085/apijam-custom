# Fidelity Postman Setup

## Collections per Business Unit

### Account Management
 * [Postman Collection](https://bitbucket.org/ra2085/apijam-custom/raw/5cdea92110f2d8a4bbc7b5c762560ee95fa5643b/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/Team/account.json)
### Benefits Registry
 * [Postman Collection](https://bitbucket.org/ra2085/apijam-custom/raw/5cdea92110f2d8a4bbc7b5c762560ee95fa5643b/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/Team/benefits.json)
### Digital Toolkit
 * [Postman Collection](https://bitbucket.org/ra2085/apijam-custom/raw/8f1dfdf742850e55d7ea3d2c81082c1d17a1c0d1/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/media/digitaltoolkit.json)
### Fidelity Access
 * [Postman Collection](https://bitbucket.org/ra2085/apijam-custom/raw/5cdea92110f2d8a4bbc7b5c762560ee95fa5643b/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/Team/fidelityaccess.json)
### Qualtrix Partner Integration
 * [Postman Collection](https://bitbucket.org/ra2085/apijam-custom/raw/defb50145beac4ccaf1946bc32dd2b1525438aef/Labs/Core/Lab%201%20API%20Design%20-%20Create%20a%20Reverse%20Proxy%20with%20OpenAPI%20specification/media/qualtrix.json)

## Import Collection instructions

1. Choose the collection that you're familiar with (per business unit).
     ![image alt text](./ps1.png)
2. Download the collection (JSON file).
	 ![image alt text](./ps2.png)
3. Save the Collection (JSON file).
	 ![image alt text](./ps3.png)
4. Find the 'Import' option in Postman.
     ![image alt text](./ps4.png)
5. Import the Collection into Postman.
	 ![image alt text](./ps5.png)