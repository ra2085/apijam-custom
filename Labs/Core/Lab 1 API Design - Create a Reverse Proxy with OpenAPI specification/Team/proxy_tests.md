## Fidelity Test Scenarios
### Account Management
#### Account
 * URL: `http://{your_org}.apigee.net/ftgw/dp/account/invalidateCache/v1`
 * HTTP Verb: `GET`
 * Headers: N/A
 * Body: N.A
 * Should expect an http 200 Success code with a DeleteCache Object in the response body.
#### Balance
 * URL: `http://{your_org}.apigee.net/ftgw/dp/balance/detail/v1`
 * HTTP Verb: `POST`
 * Headers: `Content-Type application/json`
 * Body (raw): ```{"request":{"parameter":{"acctDetails":{"acctDetail":[{"acctNum":"ullamco amet ipsum eiusmod","acctSubType":"Ut veniam","acctType":"Brokerage","hardToBorrow":true,"multiMarginSummaryInd":true,"filiSystem":"tempor in aliquip dolor"}]},"filters":{"priceTiming":{"includeRecent":true,"includeIntraday":false,"includeClose":false},"requestedData":{"includeAddlInfoDetail":true,"includeAcctValDetail":true,"includeAvailableToWithdrawDetail":false,"includeCashDetail":false,"includeBuyingPowerDetail":true,"includeMarginDetail":true,"includeBondDetail":true,"includeShortDetail":false,"includeOptionsDetail":false}}}}}```
 * Should expect an http 200 Success code with a Balances Object in the response body.
#### Position
 * URL: `http://{your_org}.apigee.net/ftgw/dp/position/v1`
 * HTTP Verb: `POST`
 * Headers: N/A
 * Body: ```{"request":{"parameter":"position": {
        "portfolioDetail": {
            "portfolioPositionCount": -8743316,
            "assetsHeldAwayPositionCount": 65509507,
            "portfolioGainLossDetail": {
                "portfolioTotalVal": 72039988.089097,
                "portfolioAssetsHeldAwayTotalVal": -4325411.138278574,
                "todaysGainLoss": -60449001.738882616,
                "totalGainLoss": -65011289.47217807,
                "todaysGainLossPct": 75136713.2109364,
                "totalGainLossPct": -32559086.73409538,
                "unadjustedTotalGainLoss": -97727099.17872265,
                "unadjustedTotalGainLossPct": -89989937.21079586,
                "unpricedSecuritiesTodaysGainLoss": 78102627.33084884,
                "pricedSecuritiesTodaysGainLoss": 1083508.7057252675,
                "costBasisTotal": -57633795.76227119
            }
        },
        "acctDetails": {
            "acctDetail": [
                {
                    "acctNum": "incididunt eiusmod sint",
                    "asOfDateTime": "exercitation",
                    "accountPositionCount": 32585806,
                    "sleeveDetails": {
                        "sleeveDetail": [
                            {
                                "id": "adipisicing qui culpa",
                                "name": "adipisicing non",
                                "description": "aute qui anim",
                                "isSeparatelyManaged": true
                            },
                            {
                                "id": "laborum enim in ipsum",
                                "name": "quis sed tempor qui",
                                "description": "eiusmod ut est ullamco in",
                                "isSeparatelyManaged": false
                            },
                            {
                                "id": "dolor nulla magna",
                                "name": "voluptate id pariatur do",
                                "description": "laboris non",
                                "isSeparatelyManaged": false
                            }
                        ]
                    },
                    "activityDetail": {
                        "unsettledCash": -2864719.115582302,
                        "unsettledMargin": -72499148.36791223,
                        "unsettledShort": 46598367.27417037,
                        "whenIssuedCash": 19910592.970946133,
                        "dividendsAccrued": 33639739.692667976,
                        "dvpRvpCash": 93001847.08022642
                    },
                    "accountGainLossDetail": {
                        "accountMarketVal": -89758961.86419043,
                        "wpsBrokerageLinkMarketVal": -16851617.37617284,
                        "assetsHeldAwayAccountMarketVal": 62698078.280551165,
                        "todaysGainLoss": {},
                        "todaysGainLossPct": {},
                        "totalGainLoss": 76267833.29666042,
                        "totalGainLossPct": 3170025.7166585624,
                        "unpricedSecuritiesTodaysGainLoss": 87580978.79012334,
                        "pricedSecuritiesTodaysGainLoss": -93569775.29718561,
                        "unadjustedTotalGainLoss": {},
                        "unadjustedTotalGainLossPct": {},
                        "sleeveBalanceDetails": {
                            "sleeveBalanceDetail": [
                                {
                                    "id": "dolore ea sint",
                                    "marketVal": 17897278.094770104,
                                    "totalGainLoss": -93795768.90514207
                                },
                                {
                                    "id": "aliqua anim id amet",
                                    "marketVal": -75701447.96911924,
                                    "totalGainLoss": -87213979.84153283
                                },
                                {
                                    "id": "proident quis magna",
                                    "marketVal": -21238579.352596566,
                                    "totalGainLoss": -49654280.54389851
                                }
                            ]
                        },
                        "costBasisTotal": 61504418.07111576
                    },
                    "positionDetails": {
                        "positionDetail": [
                            {
                                "symbol": "mollit ad irure fugiat",
                                "optionContractRootSymbol": "adipisicing ex nostrud anim",
                                "optionContractRootCusip": "minim ex",
                                "securityType": "exercitation ipsum",
                                "securitySubType": "dolore",
                                "securitySubTypeClassification": "sint laborum nisi",
                                "assetClass": "elit in qui",
                                "securityDescription": "aliqua sunt ut labore minim",
                                "cusip": "Duis anim ex ut",
                                "hasIntradayPricingInd": true,
                                "quantity": {},
                                "hasIntradayActivity": false,
                                "securityDetail": {
                                    "sedol": "culpa amet consequat nostrud",
                                    "brokerageHoldingType": "eiusmod officia",
                                    "corePositionDetail": {
                                        "isFDIC": true,
                                        "isFailedBank": true
                                    },
                                    "bondDetail": {
                                        "bondType": "occaecat in qui",
                                        "isCallable": false,
                                        "isFactorAdjusted": true,
                                        "isInflationAdjusted": true,
                                        "isInsured": false,
                                        "couponRate": 14595633.419964254,
                                        "maturityDate": "voluptate eiusmod",
                                        "hasLadder": true
                                    },
                                    "mutualFundDetail": {
                                        "isMoneyMarket": true,
                                        "isInstitutionalMoneyMarket": false
                                    },
                                    "optionDetail": {
                                        "type": "incididunt officia anim",
                                        "strikePrice": 50912346.29236016,
                                        "expirationDate": "Lorem"
                                    },
                                    "distributionDetail": {
                                        "dividendDetail": {
                                            "hasNotification": true,
                                            "executionDate": "dolore elit officia",
                                            "perShareAmount": -9429956.66529569,
                                            "payDate": "eiusm"
                                        },
                                        "shortTermCapitalGainDetail": {
                                            "hasNotification": true,
                                            "executionDate": "labore dolore elit exercitation",
                                            "perShareAmount": 26779622.067876473,
                                            "payDate": "sint ipsum dolo"
                                        },
                                        "longTermCapitalGainDetail": {
                                            "hasNotification": false,
                                            "executionDate": "eiusmod aliqua",
                                            "perShareAmount": 25673612.21929261,
                                            "payDate": "velit laboris"
                                        }
                                    },
                                    "tradeMarket": "id aliquip labore",
                                    "isNotQuotable": true,
                                    "isForeign": true,
                                    "isMargin": true,
                                    "isDvpRvp": true,
                                    "isCash": false,
                                    "isShort": false,
                                    "isLoaned": false,
                                    "isHardToBorrow": false,
                                    "isWashSaleAdjusted": false,
                                    "isFixedIncome": false,
                                    "sleeveId": "nisi pr",
                                    "investmentCode": "culpa ex ad",
                                    "isUnitized": false,
                                    "isEligibleForLots": true,
                                    "isUnsupportedCurrency": true,
                                    "currencyDetail": {
                                        "symbol": "elit cillum",
                                        "description": "reprehenderit aliquip anim"
                                    }
                                },
                                "priceDetail": {
                                    "lastPrice": {},
                                    "lastPriceChg": {},
                                    "fiftyTwoWeekHigh": {},
                                    "fiftyTwoWeekLow": {},
                                    "lastPriceChgPct": -57558749.83361142,
                                    "lastPricePreviousClose": {},
                                    "previousCloseDate": "Lorem occaecat",
                                    "currentDayTradingDetail": {
                                        "openPrice": 87695893.86117905,
                                        "bidPrice": -14057095.823318154,
                                        "askPrice": -72998838.66574746,
                                        "bidSize": -30838051.92573625,
                                        "askSize": 15089906.497796461,
                                        "volume": 98295655.81298113,
                                        "dayHighPrice": -29253735.589089617,
                                        "dayLowPrice": -41936610.56726099,
                                        "openInterest": 76108682.85669383
                                    }
                                },
                                "marketValDetail": {
                                    "marketVal": {},
                                    "previousMarketVal": {},
                                    "totalGainLossPct": {},
                                    "totalGainLoss": {},
                                    "unadjustedTotalGainLoss": {},
                                    "unadjustedTotalGainLossPct": {},
                                    "todaysGainLossPct": {},
                                    "todaysGainLoss": {},
                                    "isUnpriced": false
                                },
                                "costBasisDetail": {
                                    "avgCostPerShare": {},
                                    "unadjustedAvgCostPerShare": {},
                                    "costBasis": {},
                                    "unadjustedCostBasis": {},
                                    "type": "exercitation",
                                    "id": "tempor anim do ea",
                                    "method": "nisi in fugiat",
                                    "isAdjusted": true,
                                    "source": "mollit do esse"
                                },
                                "intraDayDetail": {
                                    "hasCalculationsPending": false,
                                    "hasMultipleTransactions": false,
                                    "isIneligibleForIntradayCalculations": false,
                                    "quantity": -71381700.10704812
                                },
                                "isin": "tempor labore aliqua",
                                "lastActionDate": "commodo Duis nisi"
                            }
                        ]
                    },
                    "fdicCoreTotal": {
                        "quantity": 34205778.81083231,
                        "value": 15227897.258001372
                    }
                }
            ]
        }
    }}```
 * Should expect an http 200 success code with a PositionReqBody Object in the response body.
### Benefits Registry
#### Client
 * URL: `http://{your_org}.apigee.net/ws-cds-client/webapi/v1/clients/45`
 * HTTP Verb: `GET`
 * Headers: N/A
 * Body: N/A
 * Should expect an http 200 Success code with a ClientsResponse Object in the response body.
#### Client Person
 * URL: `http://{your_org}.apigee.net/ws-cds-clientperson/webapi/v1/persons/any/clients/any/indicative`
 * HTTP Verb: `GET`
 * Headers: N/A
 * Body: N/A
 * Should expect an http 200 Success code with a ClientsResponse Object in the response body.
#### Person
 * URL: `http://{your_org}.apigee.net/ws-cds-person/webapi/v1/persons/id/wid`
 * HTTP Verb: `GET`
 * Headers: N/A
 * Body: N/A
 * Should expect an http 200 Success code with a PersonId Object in the response body.
#### Product Catalog
 * URL: `http://{your_org}.apigee.net/ws-cds-productcatalog/webapi/v1/product-bundles`
 * HTTP Verb: `GET`
 * Headers: N/A
 * Body: N/A
 * Should expect an http 200 Success code with a BundlesResponse Object in the response body.
### Digital Toolkit
#### Push
 * URL: `http://{your_org}.apigee.net/dev/push/api/v1/sms`
 * HTTP Verb: `POST`
 * Headers: `Content-Type application/json`
 * Body:
 ```{"phoneNo":"incididunt ipsum elit laborum enim","message":"in est ea","senderId":"dolore labore mollit"}```
 * Should expect an http 200 Success code.
### Fidelity Access
#### EFA
 * URL: `http://{your_org}.apigee.net/ftgw/dp/efa/availability/v1`
 * HTTP Verb: `GET`
 * Headers: N/A
 * Body API: N/A
### Fidelity Access
#### Qualtrix Partner Integration
 * URL: `http://{your_org}.apigee.net/SumtotalLearnerservices_WCF/ActivityMaintenanceService.svc/addUserToRoster`
 * HTTP Verb: `POST`
 * Headers: `Content-Type application/json`
 * Body API: ```{"CorpID":"nisi ut","ActivityId":"cupidatat mollit do","Status":"amet ut Excepteur Ut","Score":"nisi tempor","userId":"fugiat","password":"anim voluptate labore","ErrorCode":-57181516,"ErrorReason":"Lorem Ut"}```