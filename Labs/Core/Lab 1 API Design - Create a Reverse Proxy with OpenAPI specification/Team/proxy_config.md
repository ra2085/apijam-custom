## Fidelity API Proxies Configuration
### Account Management
#### Account
 * Proxy Name: apijam_account_v1
 * Proxy Base Path: `/ftgw/dp/account`
 * Existing API: `http://gcpconnect-test.apigee.net/ftgw/dp/account`
 * Description: Account Domain APIs
#### Balance
 * Proxy Name: apijam_balance_v1
 * Proxy Base Path: `/ftgw/dp/balance/detail`
 * Existing API: `http://gcpconnect-test.apigee.net/ftgw/dp/balance/detail`
 * Description: Balance Domain APIs
#### Position
 * Proxy Name: apijam_position_v1
 * Proxy Base Path: `/ftgw/dp/position/v1`
 * Existing API: `http://gcpconnect-test.apigee.net/ftgw/dp/position/v1`
 * Description: Position Details API
### Benefits Registry
#### Client
 * Proxy Name: apijam_client_v1
 * Proxy Base Path: `/ws-cds-client/webapi/v1`
 * Existing API: `http://gcpconnect-test.apigee.net/ws-cds-client/webapi/v1`
 * Description: Clients API
#### Client Person
 * Proxy Name: apijam_client_person_v1
 * Proxy Base Path: `/ws-cds-clientperson/webapi/v1`
 * Existing API: `http://gcpconnect-test.apigee.net/ws-cds-clientperson/webapi/v1`
 * Description: ClientPerson API
#### Person
 * Proxy Name: apijam_person_v1
 * Proxy Base Path: `/ws-cds-person/webapi/v1`
 * Existing API: `http://gcpconnect-test.apigee.net/ws-cds-person/webapi/v1`
 * Description: Person API
#### Product Catalog
 * Proxy Name: apijam_product_catalog_v1
 * Proxy Base Path: `/ws-cds-productcatalog/webapi/v1`
 * Existing API: `http://gcpconnect-test.apigee.net/ws-cds-productcatalog/webapi/v1`
 * Description: Product Catalog API
### Digital Toolkit
#### Push
 * Proxy Name: apijam_push_v1
 * Proxy Base Path: `/dev/push/api/v1`
 * Existing API: `http://gcpconnect-test.apigee.net/dev/push/api/v1`
 * Description: Dev Push API
### Fidelity Access
#### EFA
 * Proxy Name: apijam_efa_v1
 * Proxy Base Path: `/ftgw/dp/efa`
 * Existing API: `http://gcpconnect-test.apigee.net/ftgw/dp/efa`
 * Description: Durable Data API
### Qualtrix Partner Integration
#### Sum Total
* Proxy Name: **apijam_sumtotal_v1**
 * Proxy Base Path: `/SumtotalLearnerservices_WCF/ActivityMaintenanceService.svc`
 * Existing API: `http://gcpconnect-test.apigee.net/SumtotalLearnerservices_WCF/ActivityMaintenanceService.svc`
 * Description: Qualtrix Sumtotal API