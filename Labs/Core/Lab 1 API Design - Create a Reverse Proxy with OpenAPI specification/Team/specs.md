## Fidelity OpenAPI Specs
### Account Management
#### Account
 * File Name: **apijam_account_v1**
 * URL: [http://apistudio.io/7a4f9c4b-a6ed-46df-9257-c041d2d9b8fb/spec](http://apistudio.io/7a4f9c4b-a6ed-46df-9257-c041d2d9b8fb/spec)
#### Balance
 * File Name: **apijam_balance_v1**
 * URL: [http://apistudio.io/549031fa-b362-4c5a-b6ae-3314208955c8/spec](http://apistudio.io/549031fa-b362-4c5a-b6ae-3314208955c8/spec)
#### Position
 * File Name: **apijam_position_v1**
 * URL: [http://apistudio.io/466ef494-b3e3-41a3-8f5f-c86a003e251d/spec](http://apistudio.io/466ef494-b3e3-41a3-8f5f-c86a003e251d/spec)
### Benefits Registry
#### Client
* File Name: **apijam_client_v1**
* URL: [http://apistudio.io/31c426ca-5aed-4679-af0f-6a7fe135653e/spec](http://apistudio.io/31c426ca-5aed-4679-af0f-6a7fe135653e/spec)
#### Client Person
* File Name: **apijam_clientperson_v1**
* URL: [http://apistudio.io/9e0a982c-e02a-4424-bb7a-70fcb33cda19/spec](http://apistudio.io/9e0a982c-e02a-4424-bb7a-70fcb33cda19/spec)
#### Person
* File Name: **apijam_person_v1**
* URL: [http://apistudio.io/c4b23c0a-a1e6-4b55-a7a7-ebdda7fa5a76/spec](http://apistudio.io/c4b23c0a-a1e6-4b55-a7a7-ebdda7fa5a76/spec)
#### Product Catalog
* File Name: **apijam_productcatalog_v1**
* URL: [http://apistudio.io/9179e33d-3a85-4b45-8386-731af0de9ad7/spec](http://apistudio.io/9179e33d-3a85-4b45-8386-731af0de9ad7/spec)
### Digital Toolkit
#### Push
* File Name: **apijam_push_v1**
* URL: [http://apistudio.io/4e0f585d-b229-4fc2-90cb-aba6e9436281/spec](http://apistudio.io/4e0f585d-b229-4fc2-90cb-aba6e9436281/spec)
### Fidelity Access
#### EFA
* File Name: **apijam_efa_v1**
* URL: [http://apistudio.io/6442dedc-e8f4-464b-ac0c-8600205ffb7f/spec](http://apistudio.io/6442dedc-e8f4-464b-ac0c-8600205ffb7f/spec)
### Qualtrix Partner Integration
#### Sum Total
* File Name: **apijam_sumtotal_v1**
* URL: [http://apistudio.io/33a1cb4e-ed0a-4282-8ff4-88b6a521dd93/spec](http://apistudio.io/33a1cb4e-ed0a-4282-8ff4-88b6a521dd93/spec)
